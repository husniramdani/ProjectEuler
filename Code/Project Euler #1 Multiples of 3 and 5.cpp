#include<bits/stdc++.h>
using namespace std;

long long x,t,n,hasil;

long long pembagian(long long n, long long m){
    n--;
    x = n/m;
    return m*(x*(x+1))/2;
}

int main(){

    cin >> t;
    for(int i=0; i<t; i++){
        cin >> n;
        cout << pembagian(n,3) + pembagian(n,5) - pembagian(n, 15) << endl;
    }
    return 0;
}
